# Real time ISS tracker - www.101computing.net/real-time-ISS-tracker/

import json, urllib.request, datetime
import time


while True:
    # A JSON request to retrieve the current longitude and latitude of the IIS space station (real time)
    url = "http://api.open-notify.org/iss-now.json"
    response = urllib.request.urlopen(url)
    result = json.loads(response.read())

    # Let's extract the required information
    location = result["iss_position"]
    lat = location["latitude"]
    lon = location["longitude"]

    # Output informationon screen
    print("\nLatitude: " + str(lat))
    print("Longitude: " + str(lon))
    print(datetime.datetime.now())
    time.sleep(30)


