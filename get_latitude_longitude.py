import os
from math import ceil

liste = []

def read_file(file_name):
    with open(file_name) as file:
        opened_data = ""
        for bytes in range(ceil(os.path.getsize(file_name) / 1024)):

            opened_data += file.read(1024)
            if opened_data.find("\n\n") > -1:
                for i in opened_data.split('\n\n')[:-1]:
                    try:
                        data= {
                            'Latitude:': i.split('Latitude:')[1].split("\n")[0],
                            'Longitude:': i.split('Longitude:')[1].split("\n")[0]
                        }
                        liste.append(data)
                    except:
                        pass

                opened_data = opened_data[-1]

read_file('iss-verileri.txt')
print(liste)
