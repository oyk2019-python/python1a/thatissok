import csv

list6=[]

#bu fonksiyonu düzenlemek gerek
def write_cities(file_name, city_data):
    with open(file_name, "a") as file:
        file_data = ""

        for data in city_data:
            if file_data != "":
                file_data += "//"
            file_data += "{city_name}|{city_latitude}|{city_longitude}|{city_elevation}|{city_timezone}".format(
                city_name=data.get("city_name"),
                city_latitude=data.get("city_latitude"),
                city_longitude=data.get("city_longitude"),
                city_elevation=data.get("city_elevation"),
                city_timezone = data.get("city_timezone")

            )
        file_data += "//"
        file.write(file_data)

with open('cities5000.csv', newline='') as csvfile:
    read_data = csv.DictReader(csvfile, delimiter=',', quotechar='"')
    data = list(read_data)

    for row in data:
        city_name = row.get('name')
        city_latitude = row.get('latitude')
        city_longitude = row.get('longitude')
        city_elevation = row.get('elevation')
        city_timezone = row.get('timezone')
        new_data = {
            'city_name': city_name,
            'city_latitude': city_latitude,
            'city_longitude': city_longitude,
            'city_elevation': city_elevation,
            'city_timezone': city_timezone
        }
        list6.append(new_data)
        write_cities("data2",list6)
        print(new_data)