import os
import sys
import json, urllib.request, datetime
import time


class PersonData :

    name = ""
    surname = ""
    city = ""
    password = ""
    password2 = ""
    person_list=[]

    def save_file(self, file_name, user_data):
        with open(file_name, "a") as file:
            file_data = ""

            for data in user_data:
                if file_data != "":
                    file_data += "//"
                file_data += "{name}|{surname}|{city}|{password}".format(
                    name=data.get("name"),
                    surname=data.get("surname"),
                    city=data.get("city"),
                    password=data.get("password")
                )
            file_data += "//"
            file.write(file_data)

    def data_control (self):

        while True :
            self.name = input("name : ")
            self.surname = input("surname : ")
            self.city = input("city : ")
            self.password = input("password : ")
            self.password2 = input("password2 : ")

            counter=0
            if self.name.strip() == "":
                counter+=1

            if self.surname.strip() == "":
                counter+=1

            if self.city.strip() == "":
                counter+=1

            if self.password.strip() == "":
                counter+=1

            if self.password2.strip() == "":
                counter+=1

            if self.password != self.password2:
                print("Incorrect password !")
                continue

            if counter > 0 :
                print("Error ! Please fill all the information.")
                sayac = 0
                continue

            if counter == 0 :
                user_data = {"name":self.name,
                            "surname":self.surname,
                            "city":self.city,
                            "password":self.password}
                self.person_list.append(user_data)
                self.save_file("data",self.person_list)
                print("Account successfully created.")
                print("-----------------------------")
                break


    def log_in(self,password,name):
        if os.path.isfile("data"):
            with open("data") as file:
                self.person_list = file.read()
                person_list2 = self.person_list.split("//")
                counter = 0
                x=len(person_list2)
                x-=1
                counter2=0
                for person1 in person_list2:
                    person2=person1.split("|")
                    counter2+=1
                    if person2[3] == password and person2[0]==name:
                        print("welcome "+ name)
                        counter=1

                    if counter==1:
                        break

                    if x==counter2:
                        break

                if counter == 0:
                    print("User not found!")


while True:

    print("INTERNATIONAL SPACE STATION")
    print("-----------------------------")
    print("1 - Current Location  :")
    print("2 - Log in  :")
    print("3 - Create Account :")
    print("4 - Exit :")

    choice = input("Choice : ")

    person = PersonData()


    if not choice.isdigit():
        print("Incorrect entry !")
        continue

    if choice not in("1","2","3","4"):
        print("Incorrect entry !")

    if choice == "1":
        while True:
            # A JSON request to retrieve the current longitude and latitude of the IIS space station (real time)
            url = "http://api.open-notify.org/iss-now.json"
            response = urllib.request.urlopen(url)
            result = json.loads(response.read())

            # Let's extract the required information
            location = result["iss_position"]
            latitude = location["latitude"]
            longitude = location["longitude"]
            print("Date and time")
            # Output informationon screen
            print("Latitude: " + str(latitude))
            print("Longitude: " + str(longitude))
            print(datetime.datetime.now())
            choice = input("5- Exit 6- Continue : ")
            print("-----------------")
            if choice == "5":
                break
            time.sleep(30)



    if choice == "2":
        name = input("Name : ")
        password = input("Password : ")
        person.log_in(password,name)

    if choice == "3":
        person.data_control()

    if choice == "4":
        sys.exit()


